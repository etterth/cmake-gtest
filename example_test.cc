#include "example_lib.h"

#include "gtest/gtest.h"

TEST(ExampleLibTest, SaysHello) { EXPECT_EQ("HELLO", say_hello()); }
