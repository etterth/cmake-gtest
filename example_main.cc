#include "example_lib.h"

#include <iostream>

int main(int /*argc*/, char* /*argv*/[]) {
  std::cout << say_hello() << std::endl;
  return 0;
}
